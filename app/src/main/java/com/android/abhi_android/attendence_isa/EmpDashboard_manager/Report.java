package com.android.abhi_android.attendence_isa.EmpDashboard_manager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.abhi_android.attendence_isa.R;
import com.android.abhi_android.attendence_isa.Fragment_manager.Fragment_container.MainFragment;

public class Report extends AppCompatActivity implements View.OnClickListener{

    Button btLate, btLeave, btoutstanding, btsummary;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        //button
        btLate = findViewById(R.id.dailyLate);
        btLeave = findViewById(R.id.dailyLeave);
        btoutstanding = findViewById(R.id.dailyOutStation);
        btsummary = findViewById(R.id.dailySummary);

        //set onclick Listener
        btLate.setOnClickListener(this);
        btLeave.setOnClickListener(this);
        btoutstanding.setOnClickListener(this);
        btsummary.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == btLate){
            Intent intent = new Intent(getApplicationContext(), MainFragment.class);
            intent.putExtra("buttonName", "ReportLate");
            startActivity(intent);

        }else if( v == btLeave ){
            Intent intent = new Intent(getApplicationContext(), MainFragment.class);
            intent.putExtra("buttonName", "ReportLeave");
            startActivity(intent);

        }else if( v == btoutstanding){
            Intent intent = new Intent(getApplicationContext(), MainFragment.class);
            intent.putExtra("buttonName", "ReportOutstanding");
            startActivity(intent);

        }else if (v == btsummary){
            Intent intent = new Intent(getApplicationContext(), MainFragment.class);
            intent.putExtra("buttonName", "ReportSummary");
            startActivity(intent);
        }

    }
}
