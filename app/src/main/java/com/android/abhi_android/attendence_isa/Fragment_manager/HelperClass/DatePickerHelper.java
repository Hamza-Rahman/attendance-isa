package com.android.abhi_android.attendence_isa.Fragment_manager.HelperClass;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DatePickerHelper {

    private Context mcontext;

    public DatePickerHelper(Context context){
        context = this.mcontext;
    }


    //populate Calender
    public void populateDate(final TextView textView){
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker = new DatePickerDialog(mcontext, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String date = String.valueOf(dayOfMonth)+"-"+String.valueOf(monthOfYear+1)
                        +"-"+String.valueOf(year);
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date setDate = null;
                try{
                    setDate = format.parse(date);
                }
                catch (ParseException v) {
                    v.printStackTrace();

                }
                String resultDate = format.format(setDate);
                textView.setText(resultDate);
            }
        }, yy, mm, dd);
        datePicker.show();
    }

    //Add day
    public static String addDay(String oldDate) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        try {
            Date date = format.parse(oldDate);
            c.setTime(date);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DAY_OF_MONTH, 1);  //numberOfDays
        Date newDate = new Date(c.getTimeInMillis());
        String resultDate = format.format(newDate);
        return resultDate;
    }
    // Remove Day
    public static String removeDay(String oldDate){
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        try {
            Date date = format.parse(oldDate);
            c.setTime(date);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DAY_OF_MONTH, -1);  //numberOfDays
        Date newDate = new Date(c.getTimeInMillis());
        String resultDate = format.format(newDate);
        return resultDate;
    }
    public String current_date(){
        DateFormat simpleDateFormet = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        return simpleDateFormet.format(date);
    }

    public static void RemoveDayToCalender(TextView textToCalender){
        String a = textToCalender.getText().toString();
        String modifiedDate = removeDay(a);
        textToCalender.setText(modifiedDate);
    }

    public static void AddDayToCalender(TextView textToCalender){
        String a = textToCalender.getText().toString();
        String modifiedDate = addDay(a);
        textToCalender.setText(modifiedDate);
    }
}
