package com.android.abhi_android.attendence_isa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.Toast;

import com.android.abhi_android.attendence_isa.EmpDashboard_manager.Attendance;
import com.android.abhi_android.attendence_isa.EmpDashboard_manager.FFexpense;
import com.android.abhi_android.attendence_isa.EmpDashboard_manager.Leave;
import com.android.abhi_android.attendence_isa.EmpDashboard_manager.Report;
import com.android.abhi_android.attendence_isa.EmpDashboard_manager.Time_card;

public class EmpDashBoard extends AppCompatActivity implements View.OnClickListener {

    public TableRow IVreport, IVleave, IVtimecard, IVattendacne, IVffexpense;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emp_dash_board);

        //Identify ImageView Location
        IVreport = findViewById(R.id.tableItemRow_04);
        IVattendacne = findViewById(R.id.tableItemRow_01);
        IVleave = findViewById(R.id.tableItemRow_02);
        IVtimecard = findViewById(R.id.tableItemRow_03);
        IVffexpense = findViewById(R.id.tableItemRow_05);

        //set onclick listener
        IVreport.setOnClickListener(this);
        IVtimecard.setOnClickListener(this);
        IVleave.setOnClickListener(this);
        IVattendacne.setOnClickListener(this);
        IVffexpense.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (v == IVreport){
            Toast.makeText(this, "report", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(EmpDashBoard.this, Report.class);
            startActivity(intent);

        }else if(v == IVattendacne ){

            Toast.makeText(this, "attendance", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(EmpDashBoard.this, Attendance.class);
            startActivity(intent);

        }else if( v == IVleave){

            Toast.makeText(this, "leave", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(EmpDashBoard.this, Leave.class);
            startActivity(intent);

        }else if(v == IVtimecard) {

            Toast.makeText(this, "time", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(EmpDashBoard.this, Time_card.class);
            startActivity(intent);
        }else if(v == IVffexpense) {

            Toast.makeText(this, "ffexpense", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(EmpDashBoard.this, FFexpense.class);
            startActivity(intent);
        }

    }
}
