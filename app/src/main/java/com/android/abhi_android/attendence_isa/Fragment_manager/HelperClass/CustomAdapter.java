package com.android.abhi_android.attendence_isa.Fragment_manager.HelperClass;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.abhi_android.attendence_isa.Fragment_manager.Fragment.ReportLate;
import com.android.abhi_android.attendence_isa.R;
import com.android.abhi_android.attendence_isa.Fragment_manager.UserDetails.userInfo;

import java.util.ArrayList;

/**
 * Created by Hamza Rahman on 5/7/2018.
 */

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.CustomViewHolder> {

    private Context context;
    private ArrayList<String> datalist;
    private String getFragmentname;

    public CustomAdapter(Context context, ArrayList<String> datalist, String Fragmentname) {
        this.datalist = datalist;
        this.context = context;
        this.getFragmentname = Fragmentname;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View ViewFarment = null;
        switch (getFragmentname){
            case "ReportLate":
                View ReportLate = inflater.inflate(R.layout.single_leave_item_view, parent, false);
                ViewFarment = ReportLate;
                break;
            case "ExpenseCon":
                View ExpenseCon = inflater.inflate(R.layout.single_expence_con_item_view, parent, false);
                ViewFarment = ExpenseCon;
                break;
        }
        return new CustomViewHolder(ViewFarment);

    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position) {

        switch (getFragmentname){
            case "ReportLate":
                LoadLeaveReport(holder, position);
                break;
            case "ExpenseCon":
                LoadExpenseCon(holder, position);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder{

        public final View view;

        //Report Late
        TextView txtName, txtDesignation, txtIntime;

        //Expense Entry Con
        TextView expenseDetaisl, expenseTotal, expenseButton;

        public CustomViewHolder(View itemView) {
            super(itemView);
            view = itemView;

            switch (getFragmentname){
                case "ReportLate":
                    txtName = view.findViewById(R.id.userName);
                    txtDesignation = view.findViewById(R.id.userDesignation);
                    txtIntime = view.findViewById(R.id.userIntime);
                    break;
                case "ExpenseCon":
                    expenseDetaisl = view.findViewById(R.id.DetailsCon);
                    expenseTotal = view.findViewById(R.id.DetailsConTotal);
                    expenseButton = view.findViewById(R.id.DetailsConAction);
                    break;
            }
        }

    }

    public void LoadLeaveReport(CustomViewHolder holder, final int position){

        //holder.textName.setText(datalist.get(position).getData().getTitle());
        holder.txtName.setText(datalist.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "hi man", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, userInfo.class);
                //intent.putExtra("user_name", datalist.get(position));
                context.startActivity(intent);
            }
        });

        if(position % 2 == 0) {
            holder.itemView.setBackgroundColor(Color.parseColor("#e0ffdc"));
        } else {
            holder.itemView.setBackgroundColor(Color.parseColor("#f6fff5"));
        }

    }

    public void LoadExpenseCon(CustomViewHolder holder, final int position){

        //holder.textName.setText(datalist.get(position).getData().getTitle());
        holder.expenseDetaisl.setText(datalist.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "hi man", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, userInfo.class);
                //intent.putExtra("user_name", datalist.get(position));
                context.startActivity(intent);
            }
        });

        if(position % 2 == 0) {
            holder.itemView.setBackgroundColor(Color.parseColor("#e0ffdc"));
        } else {
            holder.itemView.setBackgroundColor(Color.parseColor("#f6fff5"));
        }


    }

}