package com.android.abhi_android.attendence_isa;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.abhi_android.attendence_isa.Retrofit_manager.MasterMethod.RetroMethod;
import com.android.abhi_android.attendence_isa.Retrofit_manager.Interface.Attendence_API;
import com.android.abhi_android.attendence_isa.Retrofit_manager.Model.emp;
import com.android.abhi_android.attendence_isa.Retrofit_manager.TinyDB.TinyDB;
import com.android.abhi_android.attendence_isa.Retrofit_manager.user_input.LoginData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    public static final String Base_url = "http://103.254.87.102:9090/API/API/users/";
    public Button btnGetData, btnLogin;
    public EditText etName, etPass;
    public TextView tvRegister;
    public ImageView image;

    //store Object as SharedPref
    public TinyDB tinyDB;
    public RetroMethod retroMethod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*btnGetData = (Button) findViewById(R.id.btnGetData);*/
        btnLogin = findViewById(R.id.btn_login);
        etName = findViewById(R.id.input_username);
        etPass = findViewById(R.id.input_password);
        tvRegister = findViewById(R.id.registerUser);

        final RetroMethod retro = new RetroMethod(this);

        //Login Option
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              LoginData data = new LoginData(etName.getText().toString(),etPass.getText().toString());

/*                Intent intent = new Intent(MainActivity.this, EmpMainDrawerActivity.class);
                startActivity(intent);*/

                if (checkInput()){
/*                    tinyDB.putObject("LoginData", data);
                    retro.POST("LoginData");*/
                    getDataLogin(data);
                }
            }
        });

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    //fully work perfectly DONE !!
    public void getDataLogin(LoginData data){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Base_url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Attendence_API InterfetceObj = retrofit.create(Attendence_API.class);
        Call<emp> call = InterfetceObj.Login(data.getUsername(), data.getPassword());
        call.enqueue(new Callback<emp>() {
            @Override
            public void onResponse(Call<emp> call, Response<emp> response) {

                if (response.body().getStatus().equals(true)){
                    Toast.makeText(MainActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(MainActivity.this, EmpMainDrawerActivity.class);
                    intent.putExtra("userID", response.body().getEmpId());
                    startActivity(intent);

                }else {
                    Toast.makeText(MainActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<emp> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Database Connection Problem", Toast.LENGTH_SHORT).show();
            }
        });
    }
    //check Internet Connection
    public boolean checkInternetConnection(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo =  connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
    //check Input Validation
    public boolean checkInput(){
        boolean input = false;
        if (TextUtils.isEmpty(etName.getText()) && TextUtils.isEmpty(etPass.getText())){
            Toast.makeText(this, "Please Enter username and password", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(etName.getText())){
            Toast.makeText(this, "Please Enter Username", Toast.LENGTH_SHORT).show();
        }else if(TextUtils.isEmpty(etPass.getText())){
            Toast.makeText(this, "Please Enter Password", Toast.LENGTH_SHORT).show();
        }else {
            input = true;
        }
        return input;
    }
    //check GPS Connection is active or not
    public boolean checkGPS(){
        boolean GPS = false;
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setMessage("Your GPS is Disable, want to Enable ?");
            alertDialog.setPositiveButton("Enable GPS", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent( Settings.ACTION_ACCESSIBILITY_SETTINGS );
                    startActivity(intent);
                }
            });

            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(MainActivity.this, "You have Canceled", Toast.LENGTH_SHORT).show();
                    dialog.cancel();
                }
            });
            AlertDialog alert = new AlertDialog.Builder(this).create();
            alert.show();
        }else {
            GPS = true;
            Toast.makeText(this, "your GPS is Enabled !!", Toast.LENGTH_SHORT).show();
        }
        return GPS;
    }

}
