package com.android.abhi_android.attendence_isa.EmpDashboard_manager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.abhi_android.attendence_isa.Fragment_manager.Fragment_container.MainFragment;
import com.android.abhi_android.attendence_isa.R;

public class Leave extends AppCompatActivity implements View.OnClickListener {

    Button btnLeSts, btnLeApl, btnLeChk, btnLeRecmnd, btnLeAprv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave);

        btnLeSts = findViewById(R.id.LeaveStatus);
        btnLeApl = findViewById(R.id.LeaveApply);
        btnLeChk = findViewById(R.id.LeaveCheck);
        btnLeRecmnd = findViewById(R.id.LeaveRecommend);
        btnLeAprv = findViewById(R.id.LeaveApprove);

        btnLeSts.setOnClickListener(this);
        btnLeApl.setOnClickListener(this);
        btnLeChk.setOnClickListener(this);
        btnLeRecmnd.setOnClickListener(this);
        btnLeAprv.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        Intent intent = new Intent(getApplicationContext(), MainFragment.class);
        if (v == btnLeSts){
            intent.putExtra("buttonName", "LeaveStatus");
            startActivity(intent);

        }else if( v == btnLeApl ){
            intent.putExtra("buttonName", "LeaveApply");
            startActivity(intent);

        }else if( v == btnLeChk){
            intent.putExtra("buttonName", "LeaveCheck");
            startActivity(intent);

        }else if (v == btnLeRecmnd){
            intent.putExtra("buttonName", "LeaveRecommend");
            startActivity(intent);

        }else if (v == btnLeAprv){
            intent.putExtra("buttonName", "LeaveApprove");
            startActivity(intent);

        }
    }
}
