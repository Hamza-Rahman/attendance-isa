package com.android.abhi_android.attendence_isa.Fragment_manager.Fragment;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.abhi_android.attendence_isa.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LeaveRecommend extends Fragment implements View.OnClickListener {

    View inflatedView = null;
    RecyclerView rv;
    public LeaveRecommend() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.inflatedView =  inflater.inflate(R.layout.fragment_leave_recommend, container, false);
        rv = inflatedView.findViewById(R.id.LeaveRecommend_RecyclerView);
        return inflatedView;
    }

    @Override
    public void onClick(View view) {

    }
}
