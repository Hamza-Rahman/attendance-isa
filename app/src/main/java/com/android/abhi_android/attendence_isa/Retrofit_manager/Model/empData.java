package com.android.abhi_android.attendence_isa.Retrofit_manager.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class empData {

    @SerializedName("employeeName")
    @Expose
    private String employeeName;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("organization")
    @Expose
    private String organization;
    @SerializedName("isCheckIn")
    @Expose
    private Boolean isCheckIn;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public Boolean getIsCheckIn() {
        return isCheckIn;
    }

    public void setIsCheckIn(Boolean isCheckIn) {
        this.isCheckIn = isCheckIn;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
