package com.android.abhi_android.attendence_isa.Fragment_manager.Fragment;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.abhi_android.attendence_isa.R;

public class LeaveStatus extends Fragment implements View.OnClickListener {


    public LeaveStatus() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_leave_status, container, false);
    }

    @Override
    public void onClick(View view) {

    }
}
