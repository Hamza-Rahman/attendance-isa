package com.android.abhi_android.attendence_isa.Fragment_manager.Fragment;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.abhi_android.attendence_isa.Fragment_manager.HelperClass.HelperFunction;
import com.android.abhi_android.attendence_isa.R;
import com.android.abhi_android.attendence_isa.Fragment_manager.HelperClass.CustomAdapter;
import com.android.abhi_android.attendence_isa.Fragment_manager.HelperClass.DatePickerHelper;
import com.sdsmdg.harjot.vectormaster.VectorMasterView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportLate extends Fragment implements View.OnClickListener{

    View inflatedView = null;
    public TextView tvDate, tvCompanyName;
    public VectorMasterView btNext, btPrevious;
    public DatePickerHelper datePickerHelper;
    public ArrayList<String> dateList = new ArrayList<String>();
    private CustomAdapter adapter;
    private RecyclerView recyclerView;
    private HelperFunction helperFunction;

    public ReportLate() {
        // Required empty public constructor
        //tvDate.setText();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        this.inflatedView = inflater.inflate(R.layout.fragment_late, container, false);

        helperFunction = new HelperFunction(getActivity());

        tvDate = inflatedView.findViewById(R.id.DateView);
        tvCompanyName = inflatedView.findViewById(R.id.companyList);
        btNext = inflatedView.findViewById(R.id.DateNext);
        btPrevious = inflatedView.findViewById(R.id.DatePrevious);

        tvDate.setOnClickListener(this);
        tvCompanyName.setOnClickListener(this);
        btNext.setOnClickListener(this);
        btPrevious.setOnClickListener(this);

        // adapter for Recycerview
        dateList.add(0, "Hasan");
        dateList.add(1, "Kader");
        dateList.add(2, "Limon");
        dateList.add(3, "Samsul");

        genarateDataList(dateList, inflatedView, "ReportLate");
        return inflatedView;
    }

    @Override
    public void onClick(View v) {
        if ( v == tvDate){
            helperFunction.populateDate(tvDate);
        }else if( v == tvCompanyName ){
            helperFunction.populateCompanyList(tvCompanyName);
        }else if(v == btNext ){

            String a = tvDate.getText().toString();
            String modifiedDate =  datePickerHelper.addDay(a);
            tvDate.setText(modifiedDate);

        }else if (v == btPrevious){
            String a = tvDate.getText().toString();
            String modifiedDate =  datePickerHelper.removeDay(a);
            tvDate.setText(modifiedDate);
        }

    }

    //show the date in dialog and set it on textView
    public void populateDate(){
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String date = String.valueOf(dayOfMonth) +"-"+String.valueOf(monthOfYear)
                        +"-"+String.valueOf(year);
                tvDate.setText(date);
            }
        }, yy, mm, dd);
        datePicker.show();
    }
    //show the list of company in alert dialogFragment
    public void populateCompanyList(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Company Name");

        String [] companyList= {"Transcom ERP", "Transcom INF", "TDCL Mohakhali", "TCPL Bogra", "ISA Managemnet"};
        builder.setItems(companyList, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0:
                        tvCompanyName.setText("Transcom ERP");
                        break;
                    case 1:
                        tvCompanyName.setText("Transcom INF");
                        break;
                    case 2:
                        tvCompanyName.setText("TDCL Mohakhali");
                        break;
                    case 3:
                        tvCompanyName.setText("TCPL Bogra");
                        break;
                    case 4:
                        tvCompanyName.setText("ISA Managemnet");
                        break;
                }
            }
        });
        AlertDialog dialog  = builder.create();
        dialog.show();

    }
    public void DateChange(View view) throws ParseException {
        String date = tvDate.getText().toString();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yy");
        Date convertDate  = new Date();

        Calendar c= Calendar.getInstance();

        while (view == btNext){
             Date currentDate = dateFormat.parse(date);
             //c.setFirstDayOfWeek();
             //currentDate.after()
            //addDay()
        }
    }
    public static String addDay(Date oldDate, int numberOfDays) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(dateFormat.parse(String.valueOf(oldDate)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DAY_OF_MONTH,numberOfDays);

        dateFormat=new SimpleDateFormat("MM-dd-YYYY");
        Date newDate=new Date(c.getTimeInMillis());
        String resultDate=dateFormat.format(newDate);
        return resultDate;
    }

    //for Listview
    public void genarateDataList(ArrayList<String> dataList, View v, String FrgName){

        Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();

        recyclerView = v.findViewById(R.id.lateReport_recyclerView);
        adapter = new CustomAdapter(v.getContext(), dataList, FrgName );
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(v.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
