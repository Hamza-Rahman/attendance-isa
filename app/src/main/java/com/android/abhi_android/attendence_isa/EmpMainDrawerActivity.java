package com.android.abhi_android.attendence_isa;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.abhi_android.attendence_isa.Retrofit_manager.Interface.Attendence_API;
import com.android.abhi_android.attendence_isa.Retrofit_manager.Model.empData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EmpMainDrawerActivity extends AppCompatActivity {


    TextView nameTxt, designationTxt, OrganizationTxt;
    public static final String Base_url = "http://103.254.87.102:9090/API/API/users/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_emp_main_drawer);
        nameTxt = findViewById(R.id.userNameData);
        designationTxt = findViewById(R.id.userDesignationData);
        OrganizationTxt = findViewById(R.id.OrganisationData);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EmpMainDrawerActivity.this, EmpDashBoard.class);
                startActivity(intent);
            }
        });

        Intent intent = getIntent();
        String id = intent.getStringExtra("userID");
        getDataFromDatabase(id);
    }

    public void getDataFromDatabase(String id){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Base_url).addConverterFactory(GsonConverterFactory.create()).build();
        Attendence_API attendence_api = retrofit.create(Attendence_API.class);

        Call<empData> empDataCall = attendence_api.userprofile(id);
        empDataCall.enqueue(new Callback<empData>() {
            @Override
            public void onResponse(Call<empData> call, Response<empData> response) {
                setuserInfo(response.body());
            }
            @Override
            public void onFailure(Call<empData> call, Throwable t) {

            }
        });
    }
    public void setuserInfo(empData data){
        nameTxt.setText(data.getEmployeeName());
        designationTxt.setText(data.getDesignation());

        if (data.getOrganization().equals("100000")){
            OrganizationTxt.setText("ISA");
        }else {
            OrganizationTxt.setText(data.getOrganization());
        }
    }

}
