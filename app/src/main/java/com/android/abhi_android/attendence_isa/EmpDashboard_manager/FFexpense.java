package com.android.abhi_android.attendence_isa.EmpDashboard_manager;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.abhi_android.attendence_isa.Fragment_manager.Fragment_container.FFExpenseMainFragment;
import com.android.abhi_android.attendence_isa.Fragment_manager.Fragment_container.MainFragment;
import com.android.abhi_android.attendence_isa.R;

public class FFexpense extends AppCompatActivity implements View.OnClickListener {

    Button btnFFExEnt, btnFFExChk, btnFFExRec1, btnFFExRec2, btnFFExAprv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ffexpense);

        btnFFExEnt = (Button)findViewById(R.id.FFexpenseEntry);
        btnFFExChk = (Button)findViewById(R.id.FFexpenseChk);
        btnFFExRec1 = (Button)findViewById(R.id.FFexpenseRecOne);
        btnFFExRec2 = (Button)findViewById(R.id.FFexpenseRecTwo);
        btnFFExAprv = (Button)findViewById(R.id.FFexpenseAprv);

        btnFFExEnt.setOnClickListener(this);
        btnFFExChk.setOnClickListener(this);
        btnFFExRec1.setOnClickListener(this);
        btnFFExRec2.setOnClickListener(this);
        btnFFExAprv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(), MainFragment.class);
        Intent ffintent = new Intent(getApplicationContext(), FFExpenseMainFragment.class);
        if (v == btnFFExEnt) {
            intent.putExtra("buttonName", "FFExEnt");
            startActivity(ffintent);
        }
        else if (v == btnFFExChk) {
            intent.putExtra("buttonName", "FFExChk");
            startActivity(intent);
        }
        else if (v == btnFFExRec1) {
            intent.putExtra("buttonName", "FFExRec1");
            startActivity(intent);
        }
        else if (v == btnFFExRec2) {
            intent.putExtra("buttonName", "FFExRec2");
            startActivity(intent);
        }
        else if (v == btnFFExAprv) {
            intent.putExtra("buttonName", "FFExAprv");
            startActivity(intent);
        }

    }
}
