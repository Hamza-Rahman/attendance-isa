package com.android.abhi_android.attendence_isa.Fragment_manager.FF_Fragment;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.android.abhi_android.attendence_isa.Fragment_manager.HelperClass.DatePickerHelper;
import com.android.abhi_android.attendence_isa.R;
import com.sdsmdg.harjot.vectormaster.VectorMasterView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpenseEntrySun extends Fragment implements View.OnClickListener {


    View inflatedView = null;
    public TextView tvSunEntryNo, tvSunDate;
    public VectorMasterView btSunEntryNoNext, btSunEntryNoPrev, btSunDatePrev, btSunDateNext, btSunEntryNoAdd ;
    public DatePickerHelper datePickerHelper;
    public ExpenseEntrySun() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.inflatedView = inflater.inflate(R.layout.fragment_expense_entry_sun, container, false);

        tvSunEntryNo = inflatedView.findViewById(R.id.SunEntryNoView);
        btSunEntryNoNext = inflatedView.findViewById(R.id.SunEntryNoNext);
        btSunEntryNoPrev = inflatedView.findViewById(R.id.SunEntryNoPrevious);
        btSunEntryNoAdd = inflatedView.findViewById(R.id.SunEntryNoAdd);

        tvSunDate = inflatedView.findViewById(R.id.SunDateView);
        btSunDateNext = inflatedView.findViewById(R.id.SunDateNext);
        btSunDatePrev = inflatedView.findViewById(R.id.SunDatePrevious);

        btSunEntryNoAdd.setOnClickListener(this);
        tvSunDate.setOnClickListener(this);
        btSunDateNext.setOnClickListener(this);
        btSunDatePrev.setOnClickListener(this);
        return inflatedView;

    }

    @Override
    public void onClick(View v) {
        if(v == btSunEntryNoAdd)
        {
            String a = addEntryNo(tvSunEntryNo);
            tvSunEntryNo.setText(a);
        }
        else if(v == tvSunDate)
        {
            populateDate(tvSunDate);
        }
        else if(v == btSunDateNext)
        {
            datePickerHelper.AddDayToCalender(tvSunDate);
        }
        else if(v == btSunDatePrev)
        {
            datePickerHelper.RemoveDayToCalender(tvSunDate);
        }
    }

    private String addEntryNo(TextView txtaddEntryNo) {
        int a = 0;
        String b =txtaddEntryNo.getText().toString();
        if(b.equals("*****")){
            a = 1;
            b = String.format("%05d", a);
        }
        else
        {
            a = Integer.parseInt(txtaddEntryNo.getText().toString()) + 1;
            int c = 5 - Integer.toString(a).length();
            b = String.format("%05d", a);

        }

        return b;
    }

    public void populateDate( TextView textView){
        final TextView t = textView;
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String date = String.valueOf(dayOfMonth)+"-"+String.valueOf(monthOfYear+1)
                        +"-"+String.valueOf(year);
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date setDate = null;
                try{
                    setDate = format.parse(date);
                }
                catch (ParseException v) {
                    v.printStackTrace();
                }
                String resultDate = format.format(setDate);
                t.setText(resultDate);
            }
        }, yy, mm, dd);
        datePicker.show();
    }
}
