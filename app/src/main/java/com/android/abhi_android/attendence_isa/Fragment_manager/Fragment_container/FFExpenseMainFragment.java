package com.android.abhi_android.attendence_isa.Fragment_manager.Fragment_container;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.android.abhi_android.attendence_isa.Fragment_manager.FF_Fragment.ExpenceEntryCon;
import com.android.abhi_android.attendence_isa.Fragment_manager.FF_Fragment.ExpenseEntrySun;
import com.android.abhi_android.attendence_isa.R;

public class FFExpenseMainFragment extends AppCompatActivity {
RadioButton radiobtnOne, radiobtnTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ffexpense_main_fragment);

        radiobtnOne = (RadioButton)findViewById(R.id.rdbtnCon);
        radiobtnTwo = (RadioButton)findViewById(R.id.rdbtnSun);

        radiobtnOne.setChecked(true);
        switchfragment();
        radiobtnOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchfragment();
            }
        });

        radiobtnTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radiobtnTwo.setChecked(true);
                switchfragment();
            }
        });
    }



    public void switchfragment()
    {
        if(radiobtnOne.isChecked())
        {
            FragmentTransaction fragCon = getFragmentManager().beginTransaction();
            fragCon.replace(R.id.FFFragmentContainer, new ExpenceEntryCon());
            fragCon.commit();
        }
        else if(radiobtnTwo.isChecked())
        {
            FragmentTransaction fragSun = getFragmentManager().beginTransaction();
            fragSun.replace(R.id.FFFragmentContainer, new ExpenseEntrySun());
            fragSun.commit();
        }
    }
}
