package com.android.abhi_android.attendence_isa.Fragment_manager.HelperClass;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.android.abhi_android.attendence_isa.Fragment_manager.Fragment.ReportLate;
import com.android.abhi_android.attendence_isa.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class HelperFunction {

    public CustomAdapter adapter;
    public Activity activity;

    public HelperFunction(Activity activity) {
        this.activity = activity;
    }

    //populate Calender
    public void populateDate(final TextView textView){
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker = new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String date = String.valueOf(dayOfMonth)+"-"+String.valueOf(monthOfYear+1)
                        +"-"+String.valueOf(year);
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date setDate = null;
                try{
                    setDate = format.parse(date);
                }
                catch (ParseException v) {
                    v.printStackTrace();

                }
                String resultDate = format.format(setDate);
                textView.setText(resultDate);
            }
        }, yy, mm, dd);
        datePicker.show();
    }

    //Add day
    public static void addDay(TextView dayTextView) {
        String day = dayTextView.getText().toString();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        try {
            Date date = format.parse(day);
            c.setTime(date);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DAY_OF_MONTH, 1);  //numberOfDays
        Date newDate = new Date(c.getTimeInMillis());
        dayTextView.setText(format.format(newDate));
    }
    // Remove Day
    public static void removeDay(TextView dayTextView){
        String day = dayTextView.getText().toString();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        try {
            Date date = format.parse(day);
            c.setTime(date);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DAY_OF_MONTH, -1);  //numberOfDays
        Date newDate = new Date(c.getTimeInMillis());
        dayTextView.setText(format.format(newDate));
    }

    public String current_date(){
        DateFormat simpleDateFormet = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        return simpleDateFormet.format(date);
    }

    //show the list of company in alert dialogFragment
    public void populateCompanyList(final TextView company){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Select Company Name");

        String [] companyList= {"Transcom ERP", "Transcom INF", "TDCL Mohakhali", "TCPL Bogra", "ISA Managemnet"};
        builder.setItems(companyList, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0:
                        company.setText("Transcom ERP");
                        break;
                    case 1:
                        company.setText("Transcom INF");
                        break;
                    case 2:
                        company.setText("TDCL Mohakhali");
                        break;
                    case 3:
                        company.setText("TCPL Bogra");
                        break;
                    case 4:
                        company.setText("ISA Managemnet");
                        break;
                }
            }
        });
        AlertDialog dialog  = builder.create();
        dialog.show();
    }

    //Pass Data to RecycleView
/*    public void genarateDataList(ArrayList<String> dataList, RecyclerView recyclerView){
        adapter = new CustomAdapter(activity, dataList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }*/
}
