package com.android.abhi_android.attendence_isa.Fragment_manager.Fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.abhi_android.attendence_isa.Fragment_manager.HelperClass.DatePickerHelper;
import com.android.abhi_android.attendence_isa.R;

public class ReportSummary extends Fragment implements View.OnClickListener {

    View inflatedView = null;
    public TextView  tvCompanyName;
    public DatePickerHelper datePickerHelper;
    public ReportSummary() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.inflatedView = inflater.inflate(R.layout.fragment_summary, container, false);
        tvCompanyName = inflatedView.findViewById(R.id.companyList);

        tvCompanyName.setOnClickListener(this);
        return inflatedView;
    }

    @Override
    public void onClick(View v) {
        if(v == tvCompanyName){
            populateCompanyList();
        }
    }

    public void populateCompanyList(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Company Name");
        String [] companyList= {"Transcom ERP", "Transcom INF", "TDCL Mohakhali", "TCPL Bogra", "ISA Managemnet"};
        builder.setItems(companyList, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0:
                        tvCompanyName.setText("Transcom ERP");
                        break;
                    case 1:
                        tvCompanyName.setText("Transcom INF");
                        break;
                    case 2:
                        tvCompanyName.setText("TDCL Mohakhali");
                        break;
                    case 3:
                        tvCompanyName.setText("TCPL Bogra");
                        break;
                    case 4:
                        tvCompanyName.setText("ISA Managemnet");
                        break;
                }
            }
        });
        AlertDialog dialog  = builder.create();
        dialog.show();



    }
}
