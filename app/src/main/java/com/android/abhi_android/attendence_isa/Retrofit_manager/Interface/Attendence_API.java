package com.android.abhi_android.attendence_isa.Retrofit_manager.Interface;

import com.android.abhi_android.attendence_isa.Retrofit_manager.Model.emp;
import com.android.abhi_android.attendence_isa.Retrofit_manager.Model.empData;
import com.android.abhi_android.attendence_isa.Retrofit_manager.user_input.LoginData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface Attendence_API {

    //For Login API
    @FormUrlEncoded
    @POST("login")
    Call<emp> Login(@Field("user_id") String userID, @Field("password") String pass);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("userprofile/{loginId}")
    Call<empData> userprofile(@Path("loginId") String id);

}
