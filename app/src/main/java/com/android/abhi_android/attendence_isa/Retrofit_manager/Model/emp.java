package com.android.abhi_android.attendence_isa.Retrofit_manager.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class emp {

    @SerializedName("emp_id")
    @Expose
    private String empId;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
