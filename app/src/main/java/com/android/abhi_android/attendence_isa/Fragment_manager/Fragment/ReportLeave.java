package com.android.abhi_android.attendence_isa.Fragment_manager.Fragment;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.abhi_android.attendence_isa.R;
import com.android.abhi_android.attendence_isa.Fragment_manager.HelperClass.DatePickerHelper;
import com.sdsmdg.harjot.vectormaster.VectorMasterView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportLeave extends Fragment implements View.OnClickListener{

    View inflatedView = null;
    public TextView tvDate, tvCompanyName;
    public VectorMasterView btNext, btPrevious;
    public DatePickerHelper datePickerHelper;
    public Context context;

    @Override
    public Context getContext() {
        return context;
    }

    //Date
    public ReportLeave() {
        // Required empty public constructor
        //tvDate.setText(datePickerHelper.current_date());
        //datePickerHelper.populateDate(context,tvDate);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.inflatedView = inflater.inflate(R.layout.fragment_leave, container, false);

        tvDate = inflatedView.findViewById(R.id.DateView);
        tvCompanyName = inflatedView.findViewById(R.id.companyList);
        btNext = inflatedView.findViewById(R.id.DateNext);
        btPrevious = inflatedView.findViewById(R.id.DatePrevious);

        tvDate.setOnClickListener(this);
        tvCompanyName.setOnClickListener(this);
        btNext.setOnClickListener(this);
        btPrevious.setOnClickListener(this);
        return inflatedView;
    }

    @Override
    public void onClick(View v) {
        if ( v == tvDate){
            populateDate();
        }else if( v == tvCompanyName ){
            populateCompanyList();
        }else if( v == btNext){
            String a = tvDate.getText().toString();
            String modifiedDate =  datePickerHelper.addDay(a);
            tvDate.setText(modifiedDate);
        }else if(v == btPrevious){
            String a = tvDate.getText().toString();
            String modifiedDate =  datePickerHelper.removeDay(a);
            tvDate.setText(modifiedDate);
        }
    }

    //show the date in dialog and set it on textView


    public void populateDate(){
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String date = String.valueOf(dayOfMonth)+"-"+String.valueOf(monthOfYear+1)
                        +"-"+String.valueOf(year);
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date setDate = null;
                try{
                    setDate = format.parse(date);
                }
                catch (ParseException v) {
                    v.printStackTrace();

                }
                String resultDate = format.format(setDate);
                tvDate.setText(resultDate);
            }
        }, yy, mm, dd);
        datePicker.show();
    }


    //show the list of company in alert dialogFragment
    public void populateCompanyList(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Company Name");
        String [] companyList= {"Transcom ERP", "Transcom INF", "TDCL Mohakhali", "TCPL Bogra", "ISA Managemnet"};
        builder.setItems(companyList, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0:
                        tvCompanyName.setText("Transcom ERP");
                        break;
                    case 1:
                        tvCompanyName.setText("Transcom INF");
                        break;
                    case 2:
                        tvCompanyName.setText("TDCL Mohakhali");
                        break;
                    case 3:
                        tvCompanyName.setText("TCPL Bogra");
                        break;
                    case 4:
                        tvCompanyName.setText("ISA Managemnet");
                        break;
                }
            }
        });
        AlertDialog dialog  = builder.create();
        dialog.show();



    }

}
