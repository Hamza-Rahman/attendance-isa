package com.android.abhi_android.attendence_isa.Fragment_manager.FF_Fragment;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.android.abhi_android.attendence_isa.Fragment_manager.HelperClass.DatePickerHelper;
import com.android.abhi_android.attendence_isa.R;
import com.sdsmdg.harjot.vectormaster.VectorMasterView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpenceEntryCon extends Fragment implements View.OnClickListener {

    View inflatedView = null;
    public TextView tvConEntryNo, tvConDate;
    public VectorMasterView btConEntryNoNext, btConEntryNoPrev, btConDatePrev, btConDateNext, btConEntryNoAdd ;
    public DatePickerHelper datePickerHelper;

    public ExpenceEntryCon() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.inflatedView = inflater.inflate(R.layout.fragment_expence_entry_con, container, false);

        tvConEntryNo = inflatedView.findViewById(R.id.ConEntryNoView);
        btConEntryNoNext = inflatedView.findViewById(R.id.ConEntryNoNext);
        btConEntryNoPrev = inflatedView.findViewById(R.id.ConEntryNoPrevious);
        btConEntryNoAdd = inflatedView.findViewById(R.id.ConEntryNoAdd);

        tvConDate = inflatedView.findViewById(R.id.ConDateView);
        btConDateNext = inflatedView.findViewById(R.id.ConDateNext);
        btConDatePrev = inflatedView.findViewById(R.id.ConDatePrevious);

        btConEntryNoAdd.setOnClickListener(this);
        tvConDate.setOnClickListener(this);
        btConDateNext.setOnClickListener(this);
        btConDatePrev.setOnClickListener(this);
        return inflatedView;
    }

    @Override
    public void onClick(View v) {
        if(v == btConEntryNoAdd)
        {
            String a = addEntryNo(tvConEntryNo);
            tvConEntryNo.setText(a);
        }
        else if(v == tvConDate)
        {
            populateDate(tvConDate);
        }
        else if(v == btConDateNext)
        {
            datePickerHelper.AddDayToCalender(tvConDate);
        }
        else if(v == btConDatePrev)
        {
            datePickerHelper.RemoveDayToCalender(tvConDate);
        }
    }

    private String addEntryNo(TextView txtaddEntryNo) {
        int a = 0;
        String b =txtaddEntryNo.getText().toString();
        if(b.equals("*****")){
            a = 1;
            b = String.format("%05d", a);
        }
        else
        {
            a = Integer.parseInt(txtaddEntryNo.getText().toString()) + 1;
            int c = 5 - Integer.toString(a).length();
            b = String.format("%05d", a);

        }

        return b;
    }

    public void populateDate( TextView textView){
        final TextView t = textView;
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String date = String.valueOf(dayOfMonth)+"-"+String.valueOf(monthOfYear+1)
                        +"-"+String.valueOf(year);
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date setDate = null;
                try{
                    setDate = format.parse(date);
                }
                catch (ParseException v) {
                    v.printStackTrace();
                }
                String resultDate = format.format(setDate);
                t.setText(resultDate);
            }
        }, yy, mm, dd);
        datePicker.show();
    }

}
