package com.android.abhi_android.attendence_isa.EmpDashboard_manager;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.graphics.drawable.ArgbEvaluator;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.abhi_android.attendence_isa.R;
import com.sdsmdg.harjot.vectormaster.VectorMasterView;
import com.sdsmdg.harjot.vectormaster.models.PathModel;

public class Attendance extends AppCompatActivity {

    //ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
       // imageView = findViewById(R.id.imgChk);
        final VectorMasterView vmv =(VectorMasterView) findViewById(R.id.imgChk);
        final PathModel outline = vmv.getPathModelByName("outline");
        final int[] flag = {1};
        //outline.setStrokeColor(Color.parseColor("#ED4337"));

        vmv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(flag[0] == 1)
                {
                    // initialize valueAnimator and pass start and end color values
                    @SuppressLint("RestrictedApi") ValueAnimator valueAnimator = ValueAnimator.ofObject(new ArgbEvaluator(), Color.WHITE, Color.parseColor("#19E346"));
                    valueAnimator.setDuration(1000);

                    valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator valueAnimator) {

                            // set fill color and update view
                            outline.setFillColor((Integer) valueAnimator.getAnimatedValue());
                            vmv.update();
                        }
                    });

                    valueAnimator.start();
                    flag[0] = 0;
                }
                else{
                    // initialize valueAnimator and pass start and end color values
                    @SuppressLint("RestrictedApi") ValueAnimator valueAnimator = ValueAnimator.ofObject(new ArgbEvaluator(), Color.WHITE, Color.parseColor("#ED4337"));
                    valueAnimator.setDuration(1000);

                    valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator valueAnimator) {

                            // set fill color and update view
                            outline.setFillColor((Integer) valueAnimator.getAnimatedValue());
                            vmv.update();
                        }
                    });

                    valueAnimator.start();
                    flag[0] = 1;
                }
            }
        });


/*        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
            }
        });*/
    }
}