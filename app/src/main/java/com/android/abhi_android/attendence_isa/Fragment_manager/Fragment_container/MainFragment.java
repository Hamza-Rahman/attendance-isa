package com.android.abhi_android.attendence_isa.Fragment_manager.Fragment_container;

import android.app.FragmentTransaction;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.abhi_android.attendence_isa.Fragment_manager.FF_Fragment.ExpenceEntryCon;
import com.android.abhi_android.attendence_isa.Fragment_manager.Fragment.LeaveApply;
import com.android.abhi_android.attendence_isa.Fragment_manager.Fragment.LeaveApprove;
import com.android.abhi_android.attendence_isa.Fragment_manager.Fragment.LeaveCheck;
import com.android.abhi_android.attendence_isa.Fragment_manager.Fragment.LeaveRecommend;
import com.android.abhi_android.attendence_isa.Fragment_manager.Fragment.LeaveStatus;
import com.android.abhi_android.attendence_isa.R;
import com.android.abhi_android.attendence_isa.Fragment_manager.Fragment.ReportLate;
import com.android.abhi_android.attendence_isa.Fragment_manager.Fragment.ReportLeave;
import com.android.abhi_android.attendence_isa.Fragment_manager.Fragment.ReportOutstanding;
import com.android.abhi_android.attendence_isa.Fragment_manager.Fragment.ReportSummary;

public class MainFragment extends AppCompatActivity {

    private String newString;
    public Context fragmentContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_fragment);

        //call Activity
        SwichActivity();
    }
    public void SwichActivity(){
        //get ButtonName
        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            newString= null;
        } else {
            newString= extras.getString("buttonName");
        }

        switch (newString){
            case "ReportLate":
                FragmentTransaction ftLate = getFragmentManager().beginTransaction();
                ftLate.replace(R.id.FragmentContainer, new ReportLate());
                ftLate.commit();

                break;
            case "ReportLeave":
                FragmentTransaction ftLeave = getFragmentManager().beginTransaction();
                ftLeave.replace(R.id.FragmentContainer, new ReportLeave());
                ftLeave.commit();
                break;

            case "ReportOutstanding":
                FragmentTransaction ftOutstanding = getFragmentManager().beginTransaction();
                ftOutstanding.replace(R.id.FragmentContainer, new ReportOutstanding());
                ftOutstanding.commit();
                break;

            case "ReportSummary":
                FragmentTransaction ftSummary = getFragmentManager().beginTransaction();
                ftSummary.replace(R.id.FragmentContainer, new ReportSummary());
                ftSummary.commit();
                break;

            case "LeaveStatus":
                FragmentTransaction ftLeSts = getFragmentManager().beginTransaction();
                ftLeSts.replace(R.id.FragmentContainer, new LeaveStatus());
                ftLeSts.commit();
                break;

            case "LeaveApply":
                FragmentTransaction ftLeApl = getFragmentManager().beginTransaction();
                ftLeApl.replace(R.id.FragmentContainer, new LeaveApply());
                ftLeApl.commit();
                break;

            case "LeaveRecommend":
                FragmentTransaction ftLeRecmnd = getFragmentManager().beginTransaction();
                ftLeRecmnd.replace(R.id.FragmentContainer, new LeaveRecommend());
                ftLeRecmnd.commit();
                break;

            case "LeaveCheck":
                FragmentTransaction ftLeChk = getFragmentManager().beginTransaction();
                ftLeChk.replace(R.id.FragmentContainer, new LeaveCheck());
                ftLeChk.commit();
                break;

            case "LeaveApprove":
                FragmentTransaction ftLeAprv = getFragmentManager().beginTransaction();
                ftLeAprv.replace(R.id.FragmentContainer, new LeaveApprove());
                ftLeAprv.commit();
                break;

            case "FFExEnt":
                FragmentTransaction FFExEnt = getFragmentManager().beginTransaction();
                FFExEnt.replace(R.id.FragmentContainer, new ExpenceEntryCon());
                FFExEnt.commit();
                break;

        }
    }
}
