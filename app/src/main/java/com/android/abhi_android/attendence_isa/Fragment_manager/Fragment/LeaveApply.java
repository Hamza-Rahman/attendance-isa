package com.android.abhi_android.attendence_isa.Fragment_manager.Fragment;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.android.abhi_android.attendence_isa.Fragment_manager.HelperClass.DatePickerHelper;
import com.android.abhi_android.attendence_isa.R;
import com.sdsmdg.harjot.vectormaster.VectorMasterView;
import com.sdsmdg.harjot.vectormaster.models.PathModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class LeaveApply extends Fragment implements View.OnClickListener {

    View inflatedView = null;
    public TextView tvAppDate, tvStartDate, tvEndDate, parpuse, station;
    public VectorMasterView btNext, btPrevious, btAppPrev, btAppNext, btStartNext, btStartPrev, btEndPrev, btEndNext, btnApp ;
    public DatePickerHelper datePickerHelper;
    public Context context;
    PathModel otl;

    public LeaveApply() {
        // Required empty public constructor
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflatedView = inflater.inflate(R.layout.fragment_leave_apply,container,false);

        //Application Button
        btAppNext = inflatedView.findViewById(R.id.aDateNext);
        tvAppDate  = inflatedView.findViewById(R.id.aDateView);
        btAppPrev = inflatedView.findViewById(R.id.aDatePrevious);

        //start Button
        btStartNext = inflatedView.findViewById(R.id.sDateNext);
        tvStartDate  = inflatedView.findViewById(R.id.sDateView);
        btStartPrev = inflatedView.findViewById(R.id.sDatePrevious);

        //End Button
        btEndPrev = inflatedView.findViewById(R.id.eDatePrevious);
        tvEndDate  = inflatedView.findViewById(R.id.eDateView);
        btEndNext = inflatedView.findViewById(R.id.eDateNext);


        parpuse  = inflatedView.findViewById(R.id.purpose);
        station = inflatedView.findViewById(R.id.Place);

        btnApp = inflatedView.findViewById(R.id.btnApp);

        tvAppDate.setOnClickListener(this);
        tvStartDate.setOnClickListener(this);
        tvEndDate.setOnClickListener(this);

        //Calender Button
        btAppNext.setOnClickListener(this);
        btAppPrev.setOnClickListener(this);
        btStartPrev.setOnClickListener(this);
        btStartNext.setOnClickListener(this);
        btEndNext.setOnClickListener(this);
        btEndPrev.setOnClickListener(this);

        parpuse.setOnClickListener(this);
        station.setOnClickListener(this);

        btnApp.setOnClickListener(this);

        otl = btnApp.getPathModelByName("otl");

        // Inflate the layout for this fragment
        return inflatedView;
    }

    @Override
    public void onClick(View v) {
        if ( v == tvAppDate){
            populateDate(tvAppDate);
        }else if( v == tvStartDate ){
            populateDate(tvStartDate);
        }else if( v == tvEndDate){
            populateDate(tvEndDate);
        }else if(v == btAppNext){
            //Remove Day and set To Text
            datePickerHelper.AddDayToCalender(tvAppDate);
        }else if ( v == btAppPrev){
            //Add Day and set To TextView
            datePickerHelper.RemoveDayToCalender(tvAppDate);
        }else if ( v == btStartNext){
            //Remove Day and setTo Text
            datePickerHelper.AddDayToCalender(tvStartDate);
        }else if (v == btStartPrev ){
            //Remove Day and setTo Text
            datePickerHelper.RemoveDayToCalender(tvStartDate);

        }else if(v ==btEndNext ){
            //Remove Day and setTo Text
            datePickerHelper.AddDayToCalender(tvEndDate);

        }else if (v == btEndPrev ){
            //Remove Day and setTo Text
            datePickerHelper.RemoveDayToCalender(tvEndDate);
        }else if(v == parpuse ){
            //Toast.makeText(context, "hi", Toast.LENGTH_SHORT).show();
            populatePurpose();
        }else if(v == station ){
            populatePlace();
        }
        else if(v == btnApp){
            /*Toast.makeText(getActivity(), "Applied successfully done", Toast.LENGTH_SHORT).show();*/
            @SuppressLint("RestrictedApi") ValueAnimator valueAnimator = ValueAnimator.ofFloat(0.0f, 1.0f);
            valueAnimator.setDuration(2000);

            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {

                    // set fill color and update view
                    otl.setTrimPathEnd((Float) valueAnimator.getAnimatedValue());
                    btnApp.update();
                }
            });

            valueAnimator.start();

        }
    }
    //show the date in dialog and set it on textView
    public void populateDate( TextView textView){
        final TextView t = textView;
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String date = String.valueOf(dayOfMonth)+"-"+String.valueOf(monthOfYear+1)
                        +"-"+String.valueOf(year);
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                Date setDate = null;
                try{
                    setDate = format.parse(date);
                }
                catch (ParseException v) {
                    v.printStackTrace();
                }
                String resultDate = format.format(setDate);
                t.setText(resultDate);
            }
        }, yy, mm, dd);
        datePicker.show();
    }

    public void populatePurpose(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String [] companyList= {"Family Affair", "Personal Affair", "Sick", "Traning", "Seminar", "other"};
        builder.setItems(companyList, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0:
                        parpuse.setText("Family Affair");
                        break;
                    case 1:
                        parpuse.setText("Personal Affair");
                        break;
                    case 2:
                        parpuse.setText("Sick");
                        break;
                    case 3:
                        parpuse.setText("Traning");
                        break;
                    case 4:
                        parpuse.setText("Seminar");
                        break;
                    case 5:
                        parpuse.setText("other");
                        break;
                }
            }
        });
        AlertDialog dialog  = builder.create();
        dialog.show();



    }

    public void populatePlace(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String [] companyList= {"Present Address", "Outside Home" };
        builder.setItems(companyList, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0:
                        parpuse.setText("Present Address");
                        break;
                    case 1:
                        parpuse.setText("Outside Home");
                        break;
                }
            }
        });
        AlertDialog dialog  = builder.create();
        dialog.show();
    }

}